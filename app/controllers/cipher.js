import Controller from '@ember/controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';

export default class CipherController extends Controller {
  @service cypher;
  @service swal;

  cipherMethod = null;
  @tracked message = '';
  @tracked cipherResult = '';
  @tracked cipherKey = '';
  @tracked isMethodSuccessfullyChangedAlertVisible = false;
  @tracked ae = null;

  setCipherResult(cipherResult = '') {
    this.set('cipherResult', cipherResult);
  }

  fireKeyErrorAlert() {
    this.swal.fire({
      title: 'Error!',
      text: 'Key needs to be 32 characters long or 256bit in size',
      icon: 'error',
      confirmButtonText: 'Okay'
    });
  }

  @action
  decryptMessage() {
    if (this.cipherKey.length === 32) {
      this.setCipherResult(
        this.cypher.decryptByMethod(
          this.message,
          this.cipherKey,
          this.cipherMethod
        )
      );
    } else {
      this.fireKeyErrorAlert();
    }
  }

  @action
  encryptMessage() {
    if (this.cipherKey.length === 32) {
      this.setCipherResult(
        this.cypher.encryptByMethod(
          this.message,
          this.cipherKey,
          this.cipherMethod
        )
      );
    } else {
      this.fireKeyErrorAlert();
    }
  }

  @action
  initiateAvalancheEffectTransaction() {
    this.set('ae', this.cypher.calculateAvalancheEffect(
      this.message,
      this.cipherKey,
      this.cipherMethod
    ));
  }

  setCipherMethod(cipherMethod = '') {
    this.set('cipherMethod', cipherMethod);
    this.set('isMethodSuccessfullyChangedAlertVisible', true);
  }

  @action
  setCipherMethodToAES() {
    this.setCipherMethod('AES');
  }

  @action
  setCipherMethodToDES() {
    this.setCipherMethod('DES');
  }

  @action
  setCipherMethodToTripleDES() {
    this.setCipherMethod('TripleDES');
  }
}
