import Service from '@ember/service';

export default class CypherService extends Service {
  decryptByMethod(message = '', key = '', method = '') {
    var parsedKey = CryptoJS.enc.Utf8.parse(key);
    if (method === 'AES') {
      var decryptedMessage = CryptoJS.AES.decrypt(message, parsedKey, { mode: CryptoJS.mode.ECB });
    } else if (method === 'DES') {
      var decryptedMessage = CryptoJS.DES.decrypt(message, parsedKey, { mode: CryptoJS.mode.ECB });
    } else if (method === 'TripleDES') {
      var decryptedMessage = CryptoJS.TripleDES.decrypt(message, parsedKey, { mode: CryptoJS.mode.ECB });
    } else {
      alert('Choose a decryption method');
    }

    var formattedDecryptedMessage = decryptedMessage.toString(
      CryptoJS.enc.Utf8
    );
    return formattedDecryptedMessage;
  }

  encryptByMethod(message = '', key = '', method = '') {
    var parsedKey = CryptoJS.enc.Utf8.parse(key);
    if (method === 'AES') {
      var encryptedMessage = CryptoJS.AES.encrypt(message, parsedKey, { mode: CryptoJS.mode.ECB });
    } else if (method === 'DES') {
      var encryptedMessage = CryptoJS.DES.encrypt(message, parsedKey, { mode: CryptoJS.mode.ECB });
    } else if (method === 'TripleDES') {
      var encryptedMessage = CryptoJS.TripleDES.encrypt(message, parsedKey, { mode: CryptoJS.mode.ECB });
    } else {
      alert('Choose an encryption method');
    }

    return encryptedMessage.toString();
  }

  calculateAvalancheEffect(message = '', key = '', method = '') {
    var encrypted = this.encryptByMethod(message, key, method);
    // generate a new string that has same value as message except replace its second char [
    var messageAsArray = message.split('');
    if (messageAsArray[1] != 'x') {
      messageAsArray[1] = 'x'
    } else {
      messageAsArray[1] = 'y'
    }
    var alteredMessage = messageAsArray.join('');
    // ]
    var comparativeEncrypted = this.encryptByMethod(alteredMessage, key, method);

    var encryptedBinary = this.stringToBinary(encrypted);
    var comparativeEncryptedBinary = this.stringToBinary(comparativeEncrypted);

    var arrayOfEncrypted = encryptedBinary.split('');
    var arrayOfComparativeEncrypted = comparativeEncryptedBinary.split('');
    
    var differentialCount = 0;
    for (var i = 0; i < arrayOfEncrypted.length; i++) {
      if(arrayOfEncrypted[i] != arrayOfComparativeEncrypted[i]) {
        differentialCount++;
      }
    }

    return ((differentialCount / arrayOfEncrypted.length)*100);
  }

  stringToBinary(input) {
    var characters = input.split('');
  
    return characters.map(function(char) {
      const binary = char.charCodeAt(0).toString(2)
      const pad = Math.max(8 - binary.length, 0);
      // Just to make sure it is 8 bits long.
      return '0'.repeat(pad) + binary;
    }).join('');
  }
}
